open module be.mustafa.spring.rest{
    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires java.sql;

    //REST
    requires spring.web;

    //JAXB
    requires java.xml.bind;
    requires com.sun.xml.bind;

    //Jackson
    requires jackson.annotations;

    //Validation
    requires java.validation;
}
