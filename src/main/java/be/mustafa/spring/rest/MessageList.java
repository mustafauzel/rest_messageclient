package be.mustafa.spring.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "MessageList")
public class MessageList implements Serializable {
    private List<Message> messages;

    public MessageList() {
    }

    public MessageList(List<Message> messages) {
        setMessages(messages);
    }

    @JsonProperty("Messages")
    @XmlElementWrapper(name = "Messages")
    @XmlElement(name = "Message")
    public List<Message> getMessages() {
        return messages;
    }

    private void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message message){
        this.messages.add(message);
    }

    public void removeMessage(Message message){
        this.messages.remove(message);
    }
}
