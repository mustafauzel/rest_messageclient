package be.mustafa.spring.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.StringJoiner;

@XmlRootElement(name = "Message")
@JsonPropertyOrder({"ID", "Author", "Text"})
public class Message implements Serializable {
    private int id;
    @NotBlank
    private String author;
    @NotBlank
    private String text;

    public Message() {
    }

    public Message(int id, String author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    @XmlAttribute(name = "ID")
    @JsonProperty("ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement(name = "Author")
    @JsonProperty(value = "Author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @XmlElement(name = "Text")
    @JsonProperty(value = "Text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlTransient
    @JsonIgnore
    public String getDisplayMessage(){
        return author + ": " + text;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("author='" + author + "'")
                .add("text='" + text + "'")
                .toString();
    }
}
