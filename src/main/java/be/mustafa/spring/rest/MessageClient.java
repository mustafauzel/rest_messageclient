package be.mustafa.spring.rest;

import java.net.URI;
import java.util.List;

public interface MessageClient {
    public Message getMessageById(int id);
    MessageList getMessages();
    URI createMessage(Message message);
    void updateMessage(Message message);
    Message patchMessage(int id, String text);
    void deleteMessage(int id);
}
