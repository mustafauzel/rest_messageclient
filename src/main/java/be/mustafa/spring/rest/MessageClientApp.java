package be.mustafa.spring.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MessageClientApp {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.requestFactory(HttpComponentsClientHttpRequestFactory::new).build();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(MessageClientApp.class, args);

        MessageClient messageClient = ctx.getBean(MessageClient.class);
        Message message = messageClient.getMessageById(1);
        messageClient.createMessage(new Message(0, "Dongyi", "byebye"));
        MessageList list = messageClient.getMessages();
        System.out.println(message.getText());

        System.out.println(list.getMessages());
        messageClient.updateMessage(new Message(1,"Boomer","Hello"));
        messageClient.patchMessage(1, "yo dawg");
        messageClient.deleteMessage(2);
        list = messageClient.getMessages();
        System.out.println(list.getMessages());

    }
}
