package be.mustafa.spring.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
public class MessageRestClient implements MessageClient {
    private String baseURL;
    private RestTemplate template;

    @Value("${baseURL}")
    public void setBaseURL(String baseURL){
        this.baseURL = baseURL;
    }

    @Autowired
    public void setTemplate(RestTemplate template){
        this.template = template;
    }

    @Override
    public Message getMessageById(int id) {
        ResponseEntity<Message> response =
                template.getForEntity(baseURL + "/messages/{id}",
                        Message.class, id);
        if(response.getStatusCode() == HttpStatus.OK){
            return response.getBody();
        }else{
            return null;
        }
    }

    @Override
    public MessageList getMessages() {
        ResponseEntity<MessageList> response = template.getForEntity(baseURL + "/messages", MessageList.class);
        if(response.getStatusCode() == HttpStatus.OK){
            return response.getBody();
        }else{
            return null;
        }

    }

    @Override
    public URI createMessage(Message message) {
        return template.postForLocation(baseURL + "/messages", message);
    }

    @Override
    public void updateMessage(Message message) {
        template.put(baseURL + "/messages/{0}", message, message.getId());
    }

    @Override
    public Message patchMessage(int id, String text) {
        Message message = new Message();
        message.setText(text);
        return template.patchForObject(baseURL + "/messages/{0}", message, Message.class, id);
    }

    @Override
    public void deleteMessage(int id) {
        template.delete(baseURL + "/messages/{0}", id);
    }
}
